#!/bin/bash
#
# Script bound to the play/pause button on a Logitexh S510
# Configured with the XF86AudioPlay keysym in ~/.i3/config
#

# shellcheck source=lib/bashlib.bash
source "$(dirname "${BASH_SOURCE[0]}")/lib/bashlib.bash"

bl_check_deps mpc

mpcstatus=$(mpc | awk 'NR==2 {print $1}')

if [ "${mpcstatus}" = '[playing]' ]; then
    mpc -q pause
else
    mpc -q play
fi
